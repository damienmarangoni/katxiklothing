import { Component, OnInit } from '@angular/core';
import { NAVIGATIONMENUS } from '../../common/configs/navigation-menu.config';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {

  showNavbar = false;
  dropdownSet = false;

  // Navigation menus initialization
  customClasses: Array<HighCustomClasses> = NAVIGATIONMENUS;

  constructor() { }

  ngOnInit() {
  }

  // Toggle menu on burger menu click
  toggleNavbar() {
    this.showNavbar = !this.showNavbar;
    // Set submenus to inactive
    this.customClasses.forEach(value => value.show = false);
  }

  // Active submenus
  activeMenuItem(c: HighCustomClasses) {
    c.show = !c.show;
  }
}

// Contains menu class
export interface HighCustomClasses {
  menu: string;
  class: string;
  link: string;
  submenus: Array<LowCustomClasses>;
  show: boolean;
}

// Contains menu class
export interface LowCustomClasses {
  menu: string;
  class: string;
  link: string;
}
