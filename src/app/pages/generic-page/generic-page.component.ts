import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-generic-page',
  templateUrl: './generic-page.component.html',
  styleUrls: ['./generic-page.component.scss']
})
export class GenericPageComponent implements OnInit, OnDestroy {

  sub: Subscription;
  message: string;

  constructor(private readonly route: ActivatedRoute) { }

  ngOnInit() {
    // Display name of the chosen menu
    this.sub = this.route.url.pipe(map(data => data[0].path))
    .subscribe(path => this.message = path);
  }

  ngOnDestroy() {
    // Close subscription to avoid memory leaks
    this.sub.unsubscribe();
  }
}
