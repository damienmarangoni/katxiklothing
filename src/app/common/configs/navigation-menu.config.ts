export const NAVIGATIONMENUS = [
    {menu : 'accueil', class: '', link: '/accueil', submenus: [{menu : '', class: '', link: ''}], show: false},
    {menu : 'k-shop', class: '', link: '/kshop', submenus: [{menu : 'Tee-shirts', class: '', link: ''}], show: false},
    {menu : 'Katxi', class: '', link: '/katxi', submenus: [{menu : 'Katxi Story', class: '', link: ''}], show: false},
    {menu : 'lookbook', class: '', link: '/lookbook', submenus: [{menu : '', class: '', link: ''}], show: false},
    {menu : 'mon kompte', class: '', link: '/moncompte', submenus: [{menu : '', class: '', link: ''}], show: false}
];
