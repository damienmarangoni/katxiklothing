import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenericPageComponent } from './pages/generic-page/generic-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

const routes: Routes = [
{ path: '', component: HomePageComponent },
{ path: 'moncompte', component: GenericPageComponent },
{ path: 'lookbook', component: GenericPageComponent },
{ path: 'katxi', component: GenericPageComponent },
{ path: 'kshop', component: GenericPageComponent },
{ path: 'accueil',   redirectTo: '', pathMatch: 'full' },
{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
